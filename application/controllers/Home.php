<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends Public_Controller {
    protected $id;

    public function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->model('HomeModel', 'mhome', true);
        $this->load->model('SingleModel', 'msingle', true);
        $this->load->library('pagination');
    }

    public function index() {
        $this->home();
    }

    public function home() {
        // echo "haidar chart js
        $data['title'] = "Home";
        $data['donations'] = $this->mhome->getAllDonation();
        $data['articles'] = $this->msingle->getLastArticle();
        $this->load->template('public/home', $data);
    }


    public function article() {
        $this->load->model('SingleModel', 'msingle', true);
        $data['title'] = "Article";
        $data['articles'] = $this->msingle->getLastArticle();
        $this->load->template('public/article', $data);
    }

    public function about() {
        $data['title'] = "Tentang Kami";
        $this->load->template('public/about', $data);
    }

    public function contact() {
        $data['title'] = "Hubungi Kami";
        $this->load->template('public/contact', $data);
    }

}