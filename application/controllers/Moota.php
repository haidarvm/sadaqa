<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use GuzzleHttp\Client;

class Moota extends CI_Controller {
    protected $id;

    public function __construct() {
        parent::__construct();
        $this->load->model('DonorModel', 'mdonor', true);
    }

    public function index() {
        $this->input->raw_input_stream;
        $input_data = json_decode($this->input->raw_input_stream, true);
        echo json_encode($input_data);
        if(is_array($input_data)) {
            foreach($input_data as $moota) {
                $data = $this->parse($moota);
                $this->insert_to_db($data);
            }
        } else {
            $data = $this->parse($input_data);
            $this->insert_to_db($data);
        }
    }

    public function parse($json) {
        //[ { "account_number": "8308338307", "date": "2020-06-16 18:55:33", "description": "TRF/PAY/TOP-UP ECHANNEL | KARTU 5022821002381768 0000000000000000000000 | OX360422 98334 ", "note": "", "amount": 25500, "type": "CR", "balance": 1050503, "updated_at": "2020-06-16 18:55:33", "created_at": "2020-06-16 18:55:33", "mutation_id": "32zp2GNXYWA", "token": "32zp2GNXYWA", "bank_id": "wnazGLEajGA", "bank": { "corporate_id": "ASKACTSCFH_SYH", "username": "NANDANG20", "atas_nama": "ASK ACT SERVE CONTRIBUTE FOR HUMANITY, YYS", "balance": "1050503.00", "account_number": "8308338307", "bank_type": "bniBisnis", "login_retry": 0, "date_from": "2020-06-16 00:00:00", "date_to": "2020-06-16 00:00:00", "interval_refresh": 15, "is_active": true, "in_queue": 0, "in_progress": 0, "recurred_at": "2020-06-17 13:02:35", "created_at": "2020-06-01 12:51:36", "token": "wnazGLEajGA", "bank_id": "wnazGLEajGA", "label": "BNI Bisnis", "last_update": "2020-06-16T11:55:33.000000Z" } } ]

        $data = ['account_number' =>  $json['account_number'], 'mutation_id' => $json['mutation_id'], 'date' => $json['date'],
                'description' => $json['description'], 'amount' => $json['amount'], 'type' => $json['type'], 'balance' => $json['balance'],
                'bank_id' => $json['bank_id']
                ];
        return $data;
    }

    public function insert_to_db($data) {
        $getDonor = $this->mdonor->getDonorTotal($data['amount']);
        if($getDonor->num_rows() > 0) {
            $getDonors = $getDonor->row_array();
            $data['donation_id'] = $getDonors['donation_id'];
            $this->mdonor->insertMoota($data);
            $this->send_sms($getDonors['phone'], $getDonors['total'] + $getDonors['code'], $getDonors['full_name']);
         } else {
            $this->mdonor->insertMoota($data);
        }
    }

    public function send_sms($phone, $total, $name) {
        $apikey = 'cf1475f4f9097a59ddfd42bbe91e4ffd';
        $url = 'http://sms114.xyz/sms/api_sms_otp_send_json.php';
        $senddata = array(
            'apikey' => $apikey,  
            'callbackurl' => '', 
            'datapacket'=>array()
        );
        // $number='628996834021'; 6285220089839
        $message='Jazakallah telah diterima '.$name.' sebesar '.$total;
        array_push($senddata['datapacket'],array(
            'number' => trim($phone),
            'message' => $message
        ));
        $client = new Client();
        $response = $client->post($url, [
            GuzzleHttp\RequestOptions::JSON => $senddata // or 'json' => [...]
        ]);
        // $data_response = json_decode($response->getBody(), true); // returns an array
        echo $response->getBody()->getContents();
    }

    public function sample() {
        ob_start();
        // setting 
        $apikey      = 'cf1475f4f9097a59ddfd42bbe91e4ffd'; // api key 
        $urlendpoint = 'http://sms114.xyz/sms/api_sms_otp_send_json.php'; // url endpoint api
        $callbackurl = ''; // url callback get status sms 

        // create header json  
        $senddata = array(
            'apikey' => $apikey,  
            'callbackurl' => $callbackurl, 
            'datapacket'=>array()
        );

        // create detail data json 
        // data 1
        $number='628996834021';
        $message='Haidar testing dlu aja';
        array_push($senddata['datapacket'],array(
            'number' => trim($number),
            'message' => $message
        ));
        // sending  
        $data=json_encode($senddata);
        $curlHandle = curl_init($urlendpoint);
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
        );
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 30);
        $respon = curl_exec($curlHandle);
        curl_close($curlHandle);
        header('Content-Type: application/json');
        echo $respon;
    }

}