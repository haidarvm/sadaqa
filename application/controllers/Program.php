<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Program extends Public_Controller {
    protected $id;

    public function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        $this->slug = $this->uri->segment(2);
        $this->load->model('HomeModel', 'mhome', true);
        $this->load->library('pagination');
    }

    // GET /member
    public function index() {
        $this->read();
    }

    public function all() {
        $this->load->model('SingleModel', 'msingle', true);
        $data['title'] = "Program";
        $data['donations'] = $this->mhome->getAllDonation();
        $data['articles'] = $this->msingle->getLastArticle();
        $this->load->template('public/causes', $data);
    }

    public function read() {
        // echo "haidar chart js
        $data['title'] = $this->slug;
        $data['program'] = $this->mhome->getDonationSlug($this->slug)->row();
        $data['donors'] = $this->mhome->getDonorByDonationId($data['program']->donation_id);
        $data['slides'] = $this->mhome->getSlidesBySlug($this->slug);
        $data['js'] = $this->carousel();
        $this->load->template('public/program', $data);
    }

    public function donate() {
        $data['title'] = $this->slug;
        
        $data['program'] = $this->mhome->getDonationSlug($this->slug)->row();
        // $data['js'] = $this->carousel();
        $this->load->view('public/donate', $data);
    }

    public function insert() {
        $post = $this->input->post();
        // print_r($post);exit;
        if(!empty($post['total_manual'])) {
            unset($post['total']);
            $post['total'] = $post['total_manual'];
            unset($post['total_manual']);
        } else {
            $post['total'] = $post['total'];
            unset($post['total_manual']);
        }
        // print_r($post);exit;
        $post['code'] = $this->mhome->getLastDonorNum();
        // echo $post['code'];exit;
        $post['total'] =  !empty($post['total_manual']) ? $post['total_manual'] : $post['total'] ;
        if(empty($post['total'])) {
            $this->session->set_flashdata('message', 'Mohon input nominal');
            redirect('donasi/'. $post['slug']);
        } else {
            unset($post['slug']);
            $insert_id = $this->mhome->insertDonor($post);
            // echo $insert_id;
            redirect('transfer/'. $insert_id );
        }
    }

    public function transfer($id) {
        $data['title'] = $this->slug;
        $data['donor'] = $this->mhome->getDonor($id)->row();
        // $data['js'] = $this->carousel();
        $this->load->view('public/transfer', $data);
    }


    public function carousel() {
        $javascript = "<script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            center: true,
            autoplay: true,
            autoplayTimeout: 3000,
            margin: 10,
            responsiveClass: true,
            responsive: {
              0: {
                items: 1,
                nav: false
              }
            }
        })
        </script>";
        return $javascript;
    }

} 