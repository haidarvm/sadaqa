<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Single extends Public_Controller {
    protected $id;

    public function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->model('SingleModel', 'msignle', true);
        $this->load->library('pagination');
    }


    public function read($slug) {
        // echo "haidar chart js
        $data['title'] = $slug;
        $data['article'] = $this->msignle->getArticleSlug($slug);
        $data['articles'] = $this->msignle->getLastArticle();
        $this->load->template('public/single', $data);
    }

}