<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
use Gumlet\ImageResize;

class Article extends Admin_Controller {
    protected $id;


    public function __construct() {
        parent::__construct();
        $this->load->model('ArticleModel', 'marticle', true);
    }

    // GET /member
    public function index() {
        $data['title'] = "Donasi";
        $data['articles'] = $this->marticle->getAllArticle();
        // $this->load->view("datatables");
        $this->load->admin_template('admin/article', $data);
    }


    // public function add() {
    //     // echo "haidar chart js
    //     $data['title'] = "Artikel";
    //     $data['action'] = "insert";
    //     $data['disable_jq'] = 1;
    //     $this->load->admin_template('admin/article-form', $data);
    // }

    public function add() {
        $getDraftEmpty = $this->marticle->getDraft();
        // print_r($getDraftEmpty); exit;
        if (0 == $getDraftEmpty->num_rows()) {
            // insert new id first
            $article_id = $this->marticle->newDraft();
            if (!empty($article_id)) {
                redirect('article/edit/' . $article_id);
            } else {
                return false;
            }
        } else {
            $data = $getDraftEmpty->row();
            redirect('article/edit/' . $data->article_id);
        }
        // redirect to edit_post
    }

    public function edit($article_id) {
        // echo "haidar chart js
        $data['title'] = "Donasi";
        $data['action'] = "update/".$article_id;
        $data['article'] = $this->marticle->getArticle($article_id)->row();
        $data['article_img'] = $this->marticle->getArticleImg($article_id);
        // $this->load->view("datatables");
        $data['disable_jq'] = 1;
        // print_r($data);
        $this->load->admin_template('admin/article-form', $data);
    }


    public function insert() {
        $data = $this->input->post();
        // print_r($data);exit;
        unset($data['filename']);
        $insert_id = $this->marticle->insert($data);
        redirect('article/');
    }

    public function update($article_id) {
        $data = $this->input->post();
        // print_r($data);exit;
        unset($data['filename']);
        $this->marticle->update($data, $article_id);
        redirect('article/');
    }

    public function delete($article_id) {
        // print_r($data);
        $this->mdonation->delete($article_id);
        redirect('article');
    }

    public function print() {
        $data['title'] = "print";
        $this->load->view('print', $data);
    }


    public function do_upload($id) {
        $config['upload_path'] = imgFullPath();
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = 100000;
        $config['max_width'] = 9024;
        $config['max_height'] = 9000;
        $config['file_name'] = uniqeID();
        // $config['url'] = 'testing';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('filename')) {
            $error = ['error' => $this->upload->display_errors()];
            echo imgFullPath();
            print_r($error);
        } else {
            $data['article_id'] = $id;
            $data = ['upload_data' => $this->upload->data()];
            $img_data = [
                'filename' => $data['upload_data']['file_name'],
                'article_id' => $id,
            ];
            $data_ori = [
                'article_id' => $id,
                'full_path' => imgPath() . $data['upload_data']['file_name'],
                'filename' => $data['upload_data']['file_name'],
                'original_name' => $data['upload_data']['client_name'],
                'new_site' => 1,
            ];
            // check if tb img feature not exits post id
            $image_featured_id = $this->marticle->insertImg($data_ori);
            $imgThumb = $this->resizeThumb($img_data, $image_featured_id);
            $imgMid = $this->resizeMid($img_data, $image_featured_id);
            $data['upload_data']['url'] = $imgMid;
            $data['success'] = true;
            $data['status'] = 200;

            // else
            // $data['imgMid'] = $imgMid;
            // print_r($data);
            return $data;
        }
    }

    public function do_center($id) {
        $config['upload_path'] = imgFullPath();
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = 100000;
        $config['max_width'] = 9024;
        $config['max_height'] = 9000;
        $config['file_name'] = uniqeID();
        // $config['url'] = 'testing';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('filename')) {
            $error = ['error' => $this->upload->display_errors()];
            echo imgFullPath();
            print_r($error);
        } else {
            $data['article_id'] = $id;
            $data = ['data' => $this->upload->data()];
            $img_data = [
                'filename' => $data['data']['file_name'],
                'article_id' => $id,
            ];
            $data_ori = [
                'article_id' => $id,
                'full_path' => imgFullPath() . $data['data']['file_name'],
                'filename' => $data['data']['file_name'],
                'original_name' => $data['data']['client_name'],
                'new_site' => 1,
            ];
            // check if tb img feature not exits post id
            $this->marticle->insertImgCenter($data_ori);
            $imgMid = $this->resizeMidCenter($img_data);
            $data['data']['url'] = $imgMid;
            $data['data']['link'] = $imgMid;
            $data['success'] = true;
            $data['status'] = 200;
            // print_r($data);
            return $data;
        }
    }

    public function img_upload($id) {
        $do_upload = $this->do_upload($id);
        echo json_encode($do_upload);
    }

    public function center_upload($id) {
        $do_upload = $this->do_center($id);
        echo json_encode($do_upload);
    }

    public function resizeThumb($data,$image_featured_id) {
        $source_path = imgFullPath() . $data['filename'];
        $image = new ImageResize($source_path);
        $image->crop(200, 112, true, ImageResize::CROPCENTER);
        $image->quality_jpg = 70;
        // $image->resize(200, 112, $allow_enlarge = false);
        $newfile = saveNewFile($data['filename'], 'thumb');
        $image->save($newfile, IMAGETYPE_JPEG);
        unset($data['filename']);
        // print_r($data);
        $data['img_thumb'] = imgPath() . getFileNameExt($newfile);
        $this->marticle->updateImg($data,$image_featured_id);
        return pathToUrl($newfile);
    }

    public function resizeMid($data,$image_featured_id) {
        $source_path = imgFullPath() . $data['filename'];
        $image = new ImageResize($source_path);
        // $image->crop(640, 360, true, ImageResize::CROPCENTER);
        // $image->resize(640, 360, $allow_enlarge = false);
        $image->resizeToBestFit(720, 389);
        $image->quality_jpg = 85;
        $newfile = saveNewFile($data['filename'], 'mid');
        // echo $newfile;
        $image->save($newfile, IMAGETYPE_JPEG);
        unset($data['filename']);
        $data['img_mid'] = imgPath() . getFileNameExt($newfile);
        // print_r($data);exit;
        $this->marticle->updateImg($data,$image_featured_id);
        return pathToUrl($newfile);
    }

    public function resizeMidCenter($data) {
        $source_path = imgFullPath() . $data['filename'];
        $image = new ImageResize($source_path);
        // $image->crop(640, 360, true, ImageResize::CROPCENTER);
        // $image->resize(640, 360, $allow_enlarge = false);
        $image->resizeToBestFit(720, 389);
        $image->quality_jpg = 90;
        $newfile = saveNewFile($data['filename'], 'mid');
        // echo $newfile;
        $image->save($newfile);
        unset($data['filename']);
        $data['img_mid'] = imgPath() . getFileNameExt($newfile);
        // print_r($data);exit;
        $this->marticle->updateImgCenter($data);
        return pathToUrl($newfile);
    }

    public function do_resize($filename) {
        $source_path = imgFullPath() . $filename;
        // echo $source_path;exit;
        $target_path = imgFullPath();
        $this->load->library('image_lib');
        $config_resize = [
            'image_library' => 'gd',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => true,
            'create_thumb' => true,
            'thumb_marker' => '_thumb',
            'quality' => '80%',
            'x_axis' => 100,
            'y_axis' => 60,
            'width' => 200,
            'height' => 112,
        ];
        $this->image_lib->initialize($config_resize);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        // clear //
        $this->image_lib->clear();
    }

    public function delete_img($image_featured_id, $article_id) {
        $this->marticle->deleteImg($image_featured_id);
        redirect('article/edit/'.$article_id);
    }

}