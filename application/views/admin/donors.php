

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Donasi Terakhir</h3>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Nama
                      </th>
                      <th style="width: 30%">
                          No Telp
                      </th>
                      <th>
                          Program
                      </th>
                      <th>
                          Total
                      </th>
                      <th>
                          Rekening
                      </th>
                      <th>
                          Tanngal
                      </th>
                  </tr>
              </thead>
              <tbody>
                <?php 
                $i = 1;
                foreach($donors as $row) {?>
                  <tr>
                      <td>
                          <?=$i++;?>
                      </td>
                      <td>
                            <?=$row->full_name;?>
                      </td>
                      <td>
                            <?=$row->phone;?>
                      </td>
                      <td>
                            <?=$row->title;?>
                      </td>
                      <td>
                            <?=$row->total;?>
                      </td>
                      <td>
                            <?=$row->bank_account;?>
                      </td>
                      <td>
                            <?=indoDateTime($row->created);?>
                      </td>
                  </tr>
                <?php } ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

