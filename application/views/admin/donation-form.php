
    	<link rel="stylesheet" href="<?=base_url();?>assets/css/trumbowyg.min.css">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Donasi</h3>
            </div>
            <form action="<?php echo site_url().'admin/'. $action;?>" method="POST">
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Judul Donasi</label>
                <input type="text" name="title" id="inputName" class="form-control" value="<?php echo !empty($donation) ? $donation->title : "" ;?>">
              </div>
            
              <div class="form-group">
                <label for="inputDescription">Content</label>
                    <textarea id="posteditor" class="form-control" name="description"><?php echo !empty($donation->description) ?$donation->description : '';?></textarea>
                </div>
              <!-- <div class="form-group">
                <label for="inputStatus">Status</label>
                <select class="form-control custom-select">
                  <option>Canceled</option>
                  <option>Selesai</option>
                  <option selected>Baru</option>
                </select>
              </div> -->
              <div class="form-group">
                <label for="inputName">Target</label>
                <input type="text" name="target" id="inputName" class="form-control" value="<?php echo !empty($donation) ? $donation->target : "0" ;?>">
              </div>
              <div class="form-group">
                <label for="inputName">Terkumpul</label>
                <input type="text" name="balance" id="inputName"  class="form-control" value="<?php echo !empty($donation) ? $donation->balance : "0" ;?>">
              </div>
             
                <?php 
                if(!empty($donation_img)) {
                foreach($donation_img->result() as $img) { ?>
              <div class="form-group">
                <img class="img-fluid" id="image-feature" src="<?=imgBasePathUrl($img->img_thumb);?>" /> <a href="<?=site_url().'admin/delete_img/'.$img->image_featured_id. '/'. $donation->donation_id;?>" onclick="return confirm('Apakah Anda yakin ingin menghapus?')">Delete</a>
                <img id="image-two" src="" />
              </div>
                <?php } 
                }?>
              <div class="form-group">
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add Image...</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="fileupload" type="file" name="filename" data-url="<?php echo !empty($donation) ? site_url().'admin/img_upload/'. $donation->donation_id: "";?>" multiple>
                </span>
            </div>
             <!-- The container for the uploaded files -->              
             <div class="form-group">
                <label for="postTitle" class="">Image</label>
                <div id="files" class="files"><div class="urlpath"></div></div>   
            </div>

              <a href="<?=base_url();?>admin/donation" class="btn btn-secondary">Cancel</a>
              <input type="submit" value="Save Changes" class="btn btn-success">
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        
      </div>
      
    </section>
    <!-- /.content -->
  </div>

  <?php 
$javascript = '<!-- Datatables-->

                    <script src="'.base_url() .'assets/js/jquery-3.4.1.min.js"></script>
                    <script src="'.base_url() .'assets/js/trumbowyg.min.js"></script>
                    <script src="'.base_url() .'assets/js/trumbowyg.upload.js"></script>
                    <script src="'.base_url() .'assets/js/trumbowyg.colors.min.js"></script>
                    <script src="'.base_url() .'assets/js/trumbowyg.fontsize.min.js"></script>
                    <script src="'.base_url(). 'assets/js/jquery.ui.widget.js"></script>
                    <script src="'.base_url(). 'assets/js/jquery.iframe-transport.js"></script>
                    <script src="'.base_url(). 'assets/js/jquery.fileupload.js"></script>
                    <script>
                    $(function() {
                        $("#posteditor").trumbowyg({
                            btns: [
                                ["viewHTML"],
                                ["formatting"],
                                ["strong", "em", "del"],
                                ["superscript", "subscript"],
                                ["link"],
                                ["image", "upload", "insertImage"], // Our fresh created dropdown
                                ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull"],
                                ["unorderedList", "orderedList"],
                                ["horizontalRule"],
                                ["removeformat"],
                                ["fullscreen"]
                            ],
                            imageWidthModalEdit: true,
                            lang: "id",
                            resetCss: false,
                            semantic: false,
                            tagsToRemove: [\'p\'],
                            plugins: {
                                // Add imagur parameters to upload plugin for demo purposes
                                upload: {
                                    serverPath: "'.site_url().'admin/center_upload/'. $donation->donation_id.'" ,
                                    fileFieldName: \'filename\',
                                    urlPropertyName: "data.url"
                                }
                            }
                        });
                    });
                  
                '.  
                    "
                    $(function() {
                        $('#fileupload').fileupload({
                                dataType: 'json',
                                add: function(e, data) {
                                    data.context = $('<p/>').text('Uploading...').appendTo(document.body);
                                    data.submit();
                                },
                                done: function(e, data) {
                                    data.context.text('Upload finished.');
                                }
                            }).on('fileuploadadd', function(e, data) {
                                data.context = $('<div/>').appendTo('#files');
                                $.each(data.files, function(index, file) {
                                    var node = $('<p/>').append($('<span/>').text(file.name));
                                    node.appendTo(data.context);
                                });
                            }).on('fileuploadprocessalways', function(e, data) {
                                var index = data.index,
                                    file = data.files[index],
                                    node = $(data.context.children()[index]);
                                if (file.preview) {
                                    node
                                        .prepend('<br>')
                                        .prepend(file.preview);
                                }
                                if (file.error) {
                                    node
                                        .append('<br>')
                                        .append($('<span class=\"text-danger\"/>').text(file.error));
                                }
                                if (index + 1 === data.files.length) {
                                    data.context.find('button')
                                        .text('Upload')
                                        .prop('disabled', !!data.files.error);
                                }
                            }).on('fileuploadprogressall', function(e, data) {
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                $('#progress .progress-bar').css(
                                    'width',
                                    progress + '%'
                                );
                            }).on('fileuploaddone', function(e, data) {
                                console.log('done fileuploaddone selesai');
                                data.context = $('<div/>').appendTo('#files');
                                $.each(data.result, function(index, file) {
                                    // console.log(data.result.url);
                                    console.log(file);
                                    if (file.url) {
                                        $('.urlpath').append('<img src=\"' + file.url + '\" />');
                                        $('.urlpath').append('<a href=\"' + file.url + '\">Link</a>');
                                        // console.log(file.url);
                                        var link = $('<a>').attr('target', '_blank').prop('href', file.url);
                                        var node = $('<p/>').append($('<span/>').text(file.url));
                                        node.appendTo(data.context);
                                        // console.log(data.context);
                                        $(data.context.children()[index]).wrap(link);
                                    } else if (file.error) {
                                        var error = $('<span class=\"text-danger\"/>').text(file.error);
                                        $(data.context.children()[index]).append('<br>').append(error);
                                    }
                                });
                            }).on('fileuploadfail', function(e, data) {
                                $.each(data.files, function(index) {
                                    var error = $('<span class=\"text-danger\"/>').text('File upload failed.');
                                    $(data.context.children()[index])
                                        .append('<br>')
                                        .append(error);
                                });
                            }).prop('disabled', !$.support.fileInput)
                            .parent().addClass($.support.fileInput ? undefined : 'disabled');
                    });
                    
                    function gal_temp(data) {
                        var html = '';
                        $.each(data.items, function(index, item) {
                            var imgCheck = (item.image_featured_id) + ',\'' + item.img_thumb + '\'' ;
                             html += '<a href=\"javascript:void(0)\"  onclick=\"imageCheck(' + imgCheck + ')\" class=\"img-click\" data-id=\"'+ item.image_featured_id +'\" ><img src=\"". site_url(). "assets/uploads/' + item.img_thumb + '\" class=\"gallery-item lazy\" alt=\"' +  item.post_title + '\"/></a>';
                        });
                        // console.log(html);
                        return html;
                    }
                    
                    function imageCheck(id, img) {
                        console.log(id);
                        $('input[name=\"image_featured_id\"]').val(id);
                        $('.modal').modal('hide');
                        $('#image-feature').hide();
                        $('#image-two').attr('src', '". site_url() ."assets/uploads/' + img);
                    }

                   

                    document.addEventListener(\"DOMContentLoaded\", function() {
                        var lazyloadImages = document.querySelectorAll(\"img.lazy\");    
                        var lazyloadThrottleTimeout;
                        
                        function lazyload () {
                        if(lazyloadThrottleTimeout) {
                            clearTimeout(lazyloadThrottleTimeout);
                        }    
                        
                        lazyloadThrottleTimeout = setTimeout(function() {
                            var scrollTop = window.pageYOffset;
                            lazyloadImages.forEach(function(img) {
                                if(img.offsetTop < (window.innerHeight + scrollTop)) {
                                    img.src = img.dataset.src;
                                    img.classList.remove('lazy');
                                }
                            });
                            if(lazyloadImages.length == 0) { 
                                document.removeEventListener(\"scroll\", lazyload);
                                window.removeEventListener(\"resize\", lazyload);
                                window.removeEventListener(\"orientationChange\", lazyload);
                            }
                        }, 20);
                        }
                        
                        document.addEventListener(\"scroll\", lazyload);
                        window.addEventListener(\"resize\", lazyload);
                        window.addEventListener(\"orientationChange\", lazyload);
                    });
  
  
                    "
                    
                
                    
                .'</script>';
echo $javascript;
?>
