

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Judul Donasi</h3>

         
          <h3 class="card-title float-right"><a class="btn btn-danger" href="<?=site_url();?>admin/add"> + Tambah Donasi</a></h3>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Judul
                      </th>
                      <th style="width: 30%">
                          Gambar
                      </th>
                      <th>
                          Terkumpul
                      </th>
                      <th style="width: 20%">
                           Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                <?php 
                $i = 1;
                foreach($donations as $row) {?>
                  <tr>
                      <td>
                          <?=$i++;?>
                      </td>
                      <td>
                          <a>
                              <?=$row->title;?>
                          </a>
                          <br/>
                          <small>
                            <?=$row->created;?>
                          </small>
                      </td>
                      <td>
                          <ul class="list-inline">
                              <li class="list-inline-item">
                                  <img alt="Avatar" class="" src="<?=imgBasePathUrl($row->img_thumb);?>">
                              </li>
                          </ul>
                      </td>
                      <td class="project_progress">
                          <div class="progress progress-sm">
                              <div class="progress-bar bg-green" role="progressbar" aria-volumenow="57" aria-volumemin="0" aria-volumemax="100" style="width: 57%">
                              </div>
                          </div>
                          <small>
                            Rp. <?=$row->balance;?>
                          </small>
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-primary btn-sm" href="<?=site_url().'admin/donation_view/'.$row->donation_id ?>">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href="<?=site_url().'admin/edit/'.$row->donation_id ?>">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
                <?php } ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

