</div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.4
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<?php if(!empty($disable_jq)) { 
  // echo $disable_jq?>
<?php } else {?>
<script src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<?php } ?>
<!-- Bootstrap -->
<script src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="<?=base_url();?>assets/js/adminlte.min.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="<?=base_url();?>assets/js/demo.js"></script>
</body>
</html>