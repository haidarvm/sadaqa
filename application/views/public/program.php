<!-- banner part start-->
<section class="banner_part">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-5">
                <div class="banner_text text-center">
                    <div class="banner_text_iner">
                        <h2 class="logo-text " style="max-width: 350px;"><?=$program->title;?></h2>
                        <div class="row align-items-center justify-content-center">
                            <div class="owl-carousel owl-theme owl-loaded owl-drag" style="width:350px">
                                <?php foreach($slides->result() as $slide) { ?>
                                <div class="item" style="width:350px"><img src="<?=imgBasePathUrl($slide->img_mid);?>"
                                        class="img-fluid" alt="blog">
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div id="sticky-anchor"></div>
                        <a href="<?=site_url().'donasi/'.$program->slug;?>" class="btn_2 sticky sticky-donate">Donasi
                            Sekarang</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--   banner part start-->

<!-- Start Sample Area -->
<section class="sample-text-area ">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-7">
                <h4>Target : <?=$program->target?> Terkumpul : <?=$program->balance?></h4>
                <div class="card">
                    <div class="card-header">
                        <h3>Donatur</h3>
                    </div>
                    <ul class="list-group list-group-flush">
                        <?php foreach($donors->result() as $donor) { ?>
                        <li class="list-group-item"><?php echo $donor->full_name . ' Rp'. $donor->total;?> </li>
                        <?php } ?>
                    </ul>
                </div>

                <h3 class="text-heading" style="padding-top:15px;">Cerita</h3>
                <div>
                    <?=$program->description;?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Sample Area -->