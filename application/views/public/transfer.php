<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Sadaqa Silahkan Transfer</title>

    <link href="<?=base_url();?>assets/img/logo-small.png"  rel='shortcut icon' type='image/png'>
    <!-- Icons font CSS-->
    <link href="<?=base_url();?>assets/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet"
        media="all">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/all.css">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
        rel="stylesheet">


    <!-- Main CSS-->
    <link href="<?=base_url();?>assets/css/donate.min.css" rel="stylesheet" media="all">
    <style>
    .mb-20 {
        margin-bottom: 20px;
    }

    .input-icon {
        position: relative;
    }

    .input-icon>i {
        position: absolute;
        display: block;
        transform: translate(0, -50%);
        top: 50%;
        pointer-events: none;
        width: 25px;
        text-align: center;
        font-style: normal;
    }

    .input-icon>input {
        padding-left: 25px;
        padding-right: 0;
    }

    .bank {
        height: 70px;
    }

    .bank-name {
        margin: 0;
        padding: 0;
    }

    .text-center {
        text-align: center;
    }

    .bg-gray {
        background: #e5e5e5;
        line-height: 50px
    }

    .account {
        font-weight: bold;
        display: inline;
        padding: 13px 30px;
    }

    .center {
        text-align:center;
    }

    .copy {
        background-color: blue;
        display:inline;
        padding:0 10px;
    }
    </style>
</head>

<body>
    <div class="page-wrapper bg-gra-03  p-b-50 ">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Instruksi Pembayaran</h2>
                </div>
                <div class="card-body">
                    <p class="mb-20 text-center">Transfer sesuai nominal di bawah ini:</p>
                    <h3 class="text-center mb-20"><b><?=rupiah($donor->total + $donor->code);?></b></h3>
                    <p class="mb-20 text-center">Pembayaran dilakukan ke rekening a/n</p>
                    <h4 class="mb-20 text-center"><b><?=$donor->account_name;?></b></h4>
                    <p class="mb-20 text-center">
                        <img src="<?=base_url().'assets/img/'. $donor->img ;?>" class="bank" />
                    </p>
                    <div class="center">
                        <h4 id="bank-account" class="bg-gray account"><?=$donor->bank_account;?></h4>
                        <button class="btn copy" data-clipboard-target="#bank-account">
                            SALIN
                        </button>
                    </div>
                    <br>
                    <p class="mb-20 text-center">Yuk bantu sebarkan ke keluarga</p>

                    <br>
                    <p class="p-t-45 p-b-50"><b>Umi +62 812-1132-2388 (Whatsapp)</b></p>
                    <a class="btn btn--radius-2 btn--green " href="https://api.whatsapp.com/send?phone=+6281211322388&text=Sadaqamulia-pertanyaan" type="submit">Contact WA</a>
                    <br>
                    <br>
                    <a href="<?=site_url();?>" class="btn btn--radius-2 btn--blue">Home</a>
                </div>
            </div>
        </div>
    </div>


    <script src="<?=base_url();?>assets/js/jquery-1.12.1.min.js"></script>
    <script src="<?=base_url();?>assets/js/clipboard.min.js"></script>
    <script>
    new ClipboardJS('.btn');



    $(document).ready(function() {
        $(".nominal").click(function() {
            // $("div.desc").hide();
            $(".nominal-show").show();
        });
        $(".nominal-hide").click(function() {
            // $("div.desc").hide();
            $(".nominal-show").hide();
        });
    });
    </script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->