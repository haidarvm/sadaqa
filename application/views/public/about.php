
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item text-center">
                            <h4></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    
    <!-- about part end-->
    <section class="about_us ">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6">
                    <div class="about_us_img">
                        <img src="<?=base_url();?>assets/img/post6s.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about_us_text">
                        <br/>
                        <h5>
                            Sejak tahun <br><span>2000</span>
                        </h5>
                        <h2>Tentang Sadaqamulia.id</h2>
                        <p>Berawal dari keinginan membuat gerakan sosial, pada 2013 Alfatih Timur (Timmy) 
                            membuat Sadaqamulia sebagai wadah bagi siapapun yang ingin mewujudkan proyek sosialnya.
                            Seiring waktu, Sadaqamulia bertransformasi menjadi platform galang dana dan berdonasi secara online.
                            Perjalanan tak selalu mulus, namun semangat tak pernah tergerus. Kini, 
                            Sadaqamulia telah menghubungkan lebih dari 1 juta #OrangBaik dan menyalurkan Rp 500 milIar
                            lebih donasi bagi pihak yang membutuhkan.</p>
                        <div class="banner_item">
                            <div class="single_item">
                                <h2> <span >50</span>k</h2>
                                <h5>Total
                                    Donatur</h5>
                            </div>
                            <div class="single_item">
                                <h2><span >25</span>k</h2>
                                <h5>Program Berhasil</h5>
                            </div>
                            <div class="single_item">
                                <h2><span>300</span>jt</h2>
                                <h5>Total
                                    Terkumpul</h5>
                            </div>
                        </div>
                        
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
    <!-- about part end-->
    <!--::our client part start::-->
    <section class="client_part padding_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section_tittle text-center">
                        <h2>Who Donate us</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="client_logo owl-carousel">
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-small.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-ask.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-small.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-ask.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::our client part end::-->
