<!--::footer_part start::-->
<footer class="footer_part">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-sm-6 col-lg-3">
                <div class="single_footer_part">
                    <!-- <img src="<?=base_url();?>assets/img/footer_logo.png" class="footer_logo" alt=""> -->
                    <h5>Sadaqa Mulia</h5>

                    <div class="work_hours">
                        <h5>Jam Kerja:</h5>
                        <ul>
                            <li>
                                <p> Senin - Sabtu:</p> <span> 8AM - 6PM</span>
                            </li>
                            <li>
                                <p> Minggu:</p> <span> 8AM - 12PM</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="single_footer_part footer_3">
                    <h4> Wakaf</h4>
                    <img src="<?=base_url();?>assets/img/wakaforange.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-6">
                <div class="copyright_text">
                    <P>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                        </script> All rights reserved | This template is made with <i class="ti-heart"
                            aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </P>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footer_icon social_icon">
                    <ul class="list-unstyled">
                        <li><a href="#" class="single_social_icon"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="single_social_icon"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" class="single_social_icon"><i class="fas fa-globe"></i></a></li>
                        <li><a href="#" class="single_social_icon"><i class="fab fa-behance"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--::footer_part end::-->

<!-- jquery plugins here-->

<script src="<?=base_url();?>assets/js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="<?=base_url();?>assets/js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="<?=base_url();?>assets/js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="<?=base_url();?>assets/js/swiper.min.js"></script>
<script src="<?=base_url();?>assets/js/wow.min.js"></script>
<script src="<?=base_url();?>assets/js/jquery.smooth-scroll.min.js"></script>
<!-- swiper js -->
<script src="<?=base_url();?>assets/js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="<?=base_url();?>assets/js/owl.carousel.min.js"></script>
<script src="<?=base_url();?>assets/js/jquery.nice-select.min.js"></script>
<!-- swiper js -->
<script src="<?=base_url();?>assets/js/slick.min.js"></script>
<script src="<?=base_url();?>assets/js/jquery.counterup.min.js"></script>
<script src="<?=base_url();?>assets/js/waypoints.min.js"></script>
<script src="<?=base_url();?>assets/js/countdown.jquery.min.js"></script>
<!-- <script src="<?=base_url();?>assets/js/timer.js"></script> -->
<!-- contact js -->
<script src="<?=base_url();?>assets/js/jquery.ajaxchimp.min.js"></script>
<script src="<?=base_url();?>assets/js/jquery.form.js"></script>
<script src="<?=base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url();?>assets/js/mail-script.js"></script>
<script src="<?=base_url();?>assets/js/contact.js"></script>
<!-- custom js -->
<?php if(!empty($js)) {  
   echo $js;
    }?>
<script>


function sticky_relocate_right() {
    var window_top2 = $(window).scrollTop();
    var div_top2 = $('#sticky-anchor').offset().top;
    if (window_top2 > div_top2) {
        $('.sticky-donate').addClass('mobile-sticky');
    } else {
        $('.sticky-donate').removeClass('mobile-sticky');
    }
}

$(function () {
  //$(window).addEventListener("touchmove", sticky_relocate_right, false);
//   setTimeout($(window).scroll(sticky_relocate_left),0);
  $(window).scroll(sticky_relocate_right);
//   sticky_relocate_left();
  sticky_relocate_right();
});
</script>
<script src="<?=base_url();?>assets/js/custom.js"></script>

</body>

</html>