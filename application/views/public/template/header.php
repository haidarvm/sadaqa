<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SadaqaMulia.id</title>
    <link href="<?=base_url();?>assets/img/logo-small.png"  rel='shortcut icon' type='image/png'>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/owl.theme.default.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/themify-icons.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/all.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/flaticon.css">
    <!-- magnific popup CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/magnific-popup.css">
    <!-- nice select CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/nice-select.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css?ver=1.3">
    <style>
    p {
        color: #000;
    }
    a.sticky {
        display: block;
    }
    a.sticky-donate {
        bottom:0;
        /* width:100px; */
    }
    .mobile-sticky {
        position: fixed !important;
        z-index:999;
        margin-left: 50px;
        background-color: #8d00ff;
        color: #FFF;
        bottom: 0;
        margin-bottom: 20px;
    }
    </style>
 
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <h2><a class=" logo-text"  href="<?=site_url();?>"><img src="<?=base_url();?>assets/img/logo-small.png" height="50" alt=""> Sadaqamulia.id </a></h2>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ti-menu"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item">
                                    <a class="nav-link" href="<?=site_url();?>">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?=site_url();?>about">Tentang</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="<?=site_url();?>program/all" id="navbarDropdown"
                                        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Donasi</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?=site_url();?>">Wakaf Al Quran</a>
                                        <a class="dropdown-item" href="<?=site_url();?>program/corona-dan-nasib-pengungsi-syiria">Donasi Syiria</a>
                                        <a class="dropdown-item" href="#">Donasi Palestina</a>
                                        <a class="dropdown-item" href="#">Donasi Guru Ngaji</a>
                                        <a class="dropdown-item" href="#">Donasi Masjid Nusantara</a>
                                    </div>
                                </li>

                                <li class="nav-item dropdown">
                                <a class="nav-link  dropdown-toggle" href="<?=site_url();?>home/article" id="navbarDropdown"
                                        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ruang Ustadz</a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="#">Program</a>
                                            <a class="dropdown-item" href="#">Lembar Pendaftaran</a>
                                            <a class="dropdown-item" href="#">Ruang Konsultasi</a>
                                            <a class="dropdown-item" href="#">Aktivitas Ustadz/guru ngaji</a>
                                        </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?=site_url();?>contact">Kontak</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->