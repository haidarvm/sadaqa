
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item text-center">
                            <h4></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    
    <!--::blog_part start::-->
    <section class="blog_part section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-md-8">
                    <div class="section_tittle text-center">
                        <h2>Article Terbaru</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach($articles as $article) {?>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_blog">
                        <div class="appartment_img">
                            <img src="<?=imgBasePathUrl($article->img_mid);?>" class="img-fit" alt="">
                        </div>
                        <div class="single_appartment_content">
                            <a href="<?=site_url(). 'single/read/'.$article->slug;?>">
                                <h4><?=$article->title;?></h4>
                            </a>
                            <p><?=short_desc($article->description);?> </p>
                            <ul class="list-unstyled">
                                <li><a href=""> <span class="flaticon-calendar"></span> </a> <?=indoDate($article->created);?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!--::blog_part end::-->

    <!--::our client part start::-->
    <section class="client_part padding_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section_tittle text-center">
                        <h2>Who Donate us</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="client_logo owl-carousel">
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-small.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-ask.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-small.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-ask.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::our client part end::-->
