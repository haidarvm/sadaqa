

    <!-- banner part start-->
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-7">
                    <div class="banner_text text-center">
                        <div class="banner_text_iner">
                            <h1>Bantu Sesama Dengan Sadaqa Mulia </h1>
                            <p style="color:#FFF; font-weight:bold;">Sadaqamulia.id adalah wadah untuk berdonasi dan menggalang dana secara online</p>
                            <!-- <a href="#" class="btn_2">Start Donation</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->


    

    <!--::passion part start::-->
    <section class="passion_part passion_section_padding">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-md-8">
                    <div class="section_tittle">
                        <h2>Pilihan Donasi Kami</h2>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna </p> -->

                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach($donations as $donation) { ?>
                <div class="col-sm-6 col-lg-4 col-xl-4">
                    <div class="single-home-passion">
                        <div class="card">
                            <a href="<?=site_url().'program/'.$donation->slug;?>"><img src="<?=imgBasePathUrl($donation->img_mid);?>" class="card-img-top img-fit" alt="blog" height="200"></a>
                            <div class="card-body">
                                <a href="<?=site_url().'program/'.$donation->slug;?>">
                                    <h5 class="card-title"><?=$donation->title;?></h5>
                                </a>
                                <ul>
                                    <li><img src="<?=base_url();?>assets/img/icon/passion_2.svg" alt=""> Terkumpul: <h6><?=rupiah($donation->balance);?></h6></li>
                                    <li><img src="<?=base_url();?>assets/img/icon/passion_2.svg" alt=""> Target: <h6><?=rupiah($donation->target);?></h6></li>
                                </ul>
                                <div class="skill">
                                        <div class="skill-bar skill11 wow slideInLeft animated">
                                            <span class="skill-count11"></span>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!-- <div class="col-sm-6 col-lg-4 col-xl-4">
                    <div class="single-home-passion">
                        <div class="card">
                            <img src="<?=base_url();?>assets/img/passion/passion_2.png" class="card-img-top" alt="blog">
                            <div class="card-body">
                                <a href="#">
                                    <h5 class="card-title">Fourth created forth fill
                                        created subdue be </h5>
                                </a>
                                <ul>
                                    <li><img src="<?=base_url();?>assets/img/icon/passion_2.svg" alt=""> Terkumpul: <h6>Rp 9.542.127</h6></li>
                                </ul>
                                <div class="skill">
                                        <div class="skill-bar skill11 wow slideInLeft animated">
                                            <span class="skill-count11"></span>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-4">
                    <div class="single-home-passion">
                        <div class="card">
                            <img src="<?=base_url();?>assets/img/passion/passion_3.png" class="card-img-top" alt="blog">
                            <div class="card-body">
                                <a href="#">
                                    <h5 class="card-title">Fourth created forth fill
                                        created subdue be </h5>
                                </a>
                                <ul>
                                    <li><img src="<?=base_url();?>assets/img/icon/passion_2.svg" alt=""> Terkumpul: <h6>Rp 542.127</h6></li>
                                </ul>
                                <div class="skill">
                                        <div class="skill-bar skill11 wow slideInLeft animated">
                                            <span class="skill-count11"></span>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!--::passion part end::-->

   

    <!-- about part end-->
    <section class="about_us">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6">
                    <div class="about_us_img">
                        <img src="<?=base_url();?>assets/img/post6s.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about_us_text">
                        <h5>
                            Sejak tahun <br><span>2000</span>
                        </h5>
                        <h2>Tentang Sadaqamulia.id</h2>
                        <p>Berawal dari keinginan membuat gerakan sosial, pada 2013 Alfatih Timur (Timmy) 
                            membuat Sadaqamulia sebagai wadah bagi siapapun yang ingin mewujudkan proyek sosialnya.
                            Seiring waktu, Sadaqamulia bertransformasi menjadi platform galang dana dan berdonasi secara online.
                            Perjalanan tak selalu mulus, namun semangat tak pernah tergerus. Kini, 
                            Sadaqamulia telah menghubungkan lebih dari 1 juta #OrangBaik dan menyalurkan Rp 500 milIar
                            lebih donasi bagi pihak yang membutuhkan.</p>
                        <div class="banner_item">
                            <div class="single_item">
                                <h2> <span >50</span>k</h2>
                                <h5>Total
                                    Donatur</h5>
                            </div>
                            <div class="single_item">
                                <h2><span >25</span>k</h2>
                                <h5>Program Berhasil</h5>
                            </div>
                            <div class="single_item">
                                <h2><span>300</span>jt</h2>
                                <h5>Total
                                    Terkumpul</h5>
                            </div>
                        </div>
                        
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
    <!-- about part end-->

    <!--::blog_part start::-->
    <section class="blog_part padding_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-md-8">
                    <div class="section_tittle text-center">
                        <h2>Article Terbaru</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach($articles as $article) {?>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_blog">
                        <div class="appartment_img">
                            <img src="<?=imgBasePathUrl($article->img_mid);?>" class="img-fit" alt="">
                        </div>
                        <div class="single_appartment_content">
                            <a href="<?=site_url(). 'single/read/'.$article->slug;?>">
                                <h4><?=$article->title;?></h4>
                            </a>
                            <p><?=short_desc($article->description);?> </p>
                            <ul class="list-unstyled">
                                <li><a href=""> <span class="flaticon-calendar"></span> </a> <?=indoDate($article->created);?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!--::blog_part end::-->

    <!--::our client part start::-->
    <section class="client_part padding_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section_tittle text-center">
                        <h2>Who Donate us</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="client_logo owl-carousel">
                        <div class="">
                            <img src="<?=base_url();?>assets/img/qaflogob.jpeg" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-small.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/qaflogob.jpeg" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-small.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::our client part end::-->
