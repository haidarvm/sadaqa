
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item text-center">
                            <h4></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    
    <!-- about part end-->
    <section class="about_us ">
        <div class="container">
            <div class="row justify-content-center align-items-center">
               
                <div class="col">
                    <div class="about_us_text text-center">
                       
                        <h2>Kontak Kami: </h2>
                        <h3>Sadaqa Mulia .</h3>
                        <h3>Komplek DPR Kavling F18 Cileunyi Bandung</h3>
                        <h3>022-63748048</h3>
                        <br/>
                    <img src="<?=base_url();?>assets/img/wakaforange.jpg" alt="">
                        
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
    <!-- about part end-->
    <!--::our client part start::-->
    <section class="client_part padding_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="section_tittle text-center">
                        <br/>
                        <br/>
                        <h2>Who Donate us</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="client_logo owl-carousel">
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-small.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-ask.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-small.png" alt="">
                        </div>
                        <div class="">
                            <img src="<?=base_url();?>assets/img/logo-ask.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::our client part end::-->
