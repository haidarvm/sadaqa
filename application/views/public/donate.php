<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Sadaqa <?=$program->slug;?></title>
    <link href="<?=base_url();?>assets/img/logo-small.png"  rel='shortcut icon' type='image/png'>

    <!-- Icons font CSS-->
    <link href="<?=base_url();?>assets/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet"
        media="all">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/all.css">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
        rel="stylesheet">


    <!-- Main CSS-->
    <link href="<?=base_url();?>assets/css/donate.min.css" rel="stylesheet" media="all">
    <style>
    .mb-20 {
        margin-bottom: 20px;
    }

    .input-icon {
        position: relative;
    }

    .input-icon>i {
        position: absolute;
        display: block;
        transform: translate(0, -50%);
        top: 50%;
        pointer-events: none;
        width: 25px;
        text-align: center;
        font-style: normal;
    }

    .input-icon>input {
        padding-left: 25px;
        padding-right: 0;
    }

    .bank {
        height: 40px;
    }

    .alert {
        color:red;
        font-weight:bold;
    }

    .bank-name {
        margin: 0;
        padding: 0;
    }
    </style>
</head>

<body>
    <div class="page-wrapper bg-gra-03  p-b-50 ">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title"><?=$program->title;?></h2>
                </div>
                <div class="card-body">
                    <form method="POST" id="donate" action="<?=site_url();?>program/insert">
                        <div class="form-row mb-20">
                            <div class="">
                                <label class="label label--block ">Masukan Nonimal Donasi</label>
                            </div>
                        </div>
                        <div class="row mb-20">
                            <label class="radio-container ">Rp 25.000
                                <input type="radio" class="nominal-hide" name="total" value="25000">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="row mb-20">
                            <label class="radio-container">Rp 50.000
                                <input type="radio" class="nominal-hide" name="total" value="50000">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="row mb-20">
                            <label class="radio-container">Rp 100.000
                                <input type="radio" class="nominal-hide" name="total" value="100000">
                                <span class="checkmark nominal"></span>
                            </label>
                        </div>
                            <p class="alert" class="row mb-20">
                                <?php echo $this->session->flashdata('message'); ?>
                            </p>
                        <div class="row mb-20">
                            <label class="radio-container">Nominal Lainya
                                <input type="radio" class="nominal" name="total" value="">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-row mb-20 nominal-show" style="display: none;">
                            <div class="name">Tuliskan Donasi Anda</div>
                            <div class="value">
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group-desc input-icon">
                                            <input class="input--style-5" id="total_manual" type="number"
                                                name="total_manual" min="20000"><i>Rp </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row mb-20">
                            <div class="name">Nama Lengkap</div>
                            <div class="value">
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" required type="text" name="full_name" oninvalid="this.setCustomValidity('Silahkan isi nama')"
    oninput="this.setCustomValidity('')">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row mb-20">
                            <div class="name">No Telp</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" required name="phone" oninvalid="this.setCustomValidity('Silahkan isi no Hp')"
    oninput="this.setCustomValidity('')">
                                </div>
                            </div>
                        </div>
                        <div class="form-row mb-20">
                            <div class="">
                                <label class="label label--block ">Metode Pembayaran Donasi</label>
                            </div>
                        </div>
                        <div class="row mb-20">
                            <label class="radio-container"><img src="<?=base_url();?>assets/img/bni-syariah.png"
                                    class="bank" /><span class="bank-name">BNI Syariah</span>
                                <input type="radio" checked="checked" name="bank_id" value="2">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="row mb-20">
                            <label class="radio-container"><img src="<?=base_url();?>assets/img/mandiri.png"
                                    class="bank" /><span class="bank-name">Mandiri</span>
                                <input type="radio" name="bank_id" value="1">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="row mb-20">
                            <label class="radio-container"><img src="<?=base_url();?>assets/img/muamalat.png"
                                    class="bank" /><span class="bank-name">&nbsp;&nbsp;&nbsp;&nbsp;Muamalat</span>
                                <input type="radio" name="bank_id" value="3">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div>
                            <input type="hidden" name="donation_id" value="<?=$program->donation_id?>">
                            <input type="hidden" name="slug" value="<?=$program->slug?>">
                            <button class="btn btn--radius-2 btn--red" type="submit">Lanjut Pembayaran</button>
                        </div>
                    </form>
                    <br>
                    <p class="p-t-45 p-b-50"><b>Umi +62 812-1132-2388 (Whatsapp)</b></p>
                    <a class="btn btn--radius-2 btn--green "
                        href="https://api.whatsapp.com/send?phone=+6281211322388&text=Sadaqamulia-pertanyaan">Contact WA</a>
                    <br>
                    <br>
                    <a href="<?=site_url();?>" class="btn btn--radius-2 btn--blue">Home</a>
                </div>
            </div>
        </div>
    </div>


    <script src="<?=base_url();?>assets/js/jquery-1.12.1.min.js"></script>
    <script>
    $(document).ready(function() {
        // $("#donate").submit(function(){
        //     console.log($(".nominal-hide").val());
        //     if (!$('#nominal-hide').is(":checked") || !$(".nominal").is(":checked")) { 
        //         console.log("kosong nominalnya");
        //         // alert("kosong");
        //         $(".alert").html("Silahkan Pilih nominal nya");
        //     } else if($(".nominal").is(":checked") && $(".total_manual").val() == 0) {
        //         $(".alert").html("Silahkan masukan nominal nya");
        //     }
        //     // if($(".nominal-show").val() == 0 && $(".nominal-hide").val() == 0) {
        //     //     $(".alert").append("Silahkan input nominal");
        //     //     $(".nominal-show").prop('required',true);
        //     //     return false
        //     // }
        //     return false;
        // });
        $(".nominal").click(function() {
            // $("div.desc").hide();
            $(".nominal-show").show();

        });
        $(".nominal-hide").click(function() {
            // $("div.desc").hide();
            $(".nominal-show").hide();
            $('#total_manual').val('');
        });
    });
    </script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->