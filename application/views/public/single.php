

   <!-- breadcrumb start-->
   <section class="breadcrumb breadcrumb_bg">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="breadcrumb_iner">
                  <div class="breadcrumb_iner_item text-center">
                     <h4></h4>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- breadcrumb start-->

   <!--================Blog Area =================-->
   <section class="blog_area single-post-area section_padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 posts-list">
               <div class="single-post">
                  <h2><?=$article->title;?></h2>
                  <div class="feature-img">
                     <img class="img-fluid" src="<?=imgBasePathUrl($article->full_path);?>" alt="">
                  </div>
                  <div class="blog_details">
                    <?=$article->description;?>
                  </div>
               </div>
                
            </div>
            <div class="col-lg-4">
               <div class="blog_right_sidebar">
                  <aside class="single_sidebar_widget popular_post_widget">
                     <h3 class="widget_title">Article Terbaru</h3>
                     <?php foreach($articles as $row) { ?>
                     <div class="media post_item">
                        <img src="<?=imgBasePathUrl($row->img_thumb);?>" width="80" alt="post">
                        <div class="media-body">
                           <a href="single-blog.html">
                              <h3><?=$row->title;?></h3>
                           </a>
                           <p><?=indoDate($row->created);?></p>
                        </div>
                     </div>
                     <?php } ?>
                  </aside>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================Blog Area end =================-->
