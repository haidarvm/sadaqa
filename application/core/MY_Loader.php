<?php 
class MY_Loader extends CI_Loader
{


    public function template($template_name, $vars = [], $return = false)
    {
        if ($return) {
            $content = $this->view('public/template/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('public/template/footer', $vars, $return);
            return $content;
        }
        $this->view('public/template/header', $vars);
        $this->view($template_name, $vars);
        // $vars['javascript'] = $this->javascript;
        $this->view('public/template/footer', $vars);
    }

    public function admin_template($template_name, $vars = [], $return = false) {
        // $vars['disable_jq'] = $this->disable_jq;
        if ($return) {
            $content = $this->view('admin/template/header', $vars, $return);
            // $content .= $this->view('templates/sidebar', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('admin/template/footer', $vars, $return);
            return $content;
        }
        $this->view('admin/template/header', $vars);
        $this->view($template_name, $vars);
        // print_r($vars);
        $this->view('admin/template/footer', $vars);
    }


}