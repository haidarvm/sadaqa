<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AuthModel extends CI_Model {
    private $tabelUser = 'users';
    private $tablePosts = 'posts';


    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function checkUser($username, $password) {
        $query = $this->db->get_where($this->tabelUser, ['user_login' => strtolower($username), 'user_pass' => $password], $limit = 1);
        // echo $this->db->last_query();exit;
        return $query;
    }

    public function getUser($userid) {
        $query = $this->db->get_where($this->tabelUser, ['ID' => $userid], $limit = 1);
        return $query->row();
    }


    public function moveUploadedFile($directory, $filepath) {
        // $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $basename = md5(mt_rand(100000, 999999));
        $filename = sprintf('%s.%0.8s', $basename, $filepath);
        $uploadedFile->moveTo($directory.DIRECTORY_SEPARATOR.$filename);
        return $filename;
    }

    // public function updateUser($userid, $data) {
    //     if (empty($data['password'])) {
    //         unset($data['password']);
    //     } else {
    //         $data['password'] = md5($data['password']);
    //     }
    //     $query = $this->db->update($this->tabelUser, $data, ['uid' => $userid]);
    //     return $query;
    // }

    public function logout() {
        $this->session->unset_userdata(['username' => '', 'login' => false]);
        $this->session->sess_destroy();
    }

    public function getFuturePosts() {
        $this->db->select('post_date,ID,post_title');
        $this->db->order_by('ID', 'DESC');
        $this->db->where('post_date <=', 'NOW()', false);
        $this->db->where('post_date >=', 'DATE_SUB(NOW(), INTERVAL 7 MINUTE)', false);
        $query = $this->db->get_where($this->tablePosts, ['post_status' => 'future']);
        // echo $this->db->last_query(); 
        //exit;
        if($query->num_rows() > 0) {
            return $query->result();
        } else {
            return 0;
        }
    }

    public function setPublishFuturePosts($ID) {
        $this->db->update($this->tablePosts, ['post_status' => "publish"], ['ID' => $ID]);
        return $this->db->affected_rows();
    }

    public function listCol() {
        $sql = "SELECT `COLUMN_NAME` 
        FROM `INFORMATION_SCHEMA`.`COLUMNS` 
        WHERE `TABLE_SCHEMA`='apibtt' 
            AND `TABLE_NAME`='btt_tblUser';";
        $query = $this->db->query($sql);
        return $query->result();
    }

    
}
