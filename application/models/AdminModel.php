
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AdminModel extends CI_Model {
    private $tb_posts = 'posts';
    private $tb_list_category = 'list_category';
    private $tb_keyword = 'keyword';
    private $tb_popular_posts = 'popular_posts';
    private $tb_category = 'category';
    private $tb_image_featured = 'image_featured';
    private $tb_image_center = 'image_center';
    private $tb_video = 'video';

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }


    public function insertImg($data) {
        $this->db->insert($this->tb_image_featured, $data);
        return $this->db->insert_id();
    }

    public function insertImgCenter($data) {
        $this->db->insert($this->tb_image_center, $data);
        return $this->db->insert_id();
    }

    public function updateImg($data,$image_featured_id) {
        // $id = $data['donation_id'];
        // unset($data['donation_id']);
        $query = $this->db->update($this->tb_image_featured, $data, ['image_featured_id' => $image_featured_id]);
        // echo  $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function updateImgCenter($data) {
        $id = $data['donation_id'];
        unset($data['donation_id']);
        $query = $this->db->update($this->tb_image_center, $data, ['donation_id' => $id]);
        // echo  $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function deleteImg($image_featured_id) {
        return $this->db->delete('image_featured', ['image_featured_id' => $image_featured_id]);
    }

}