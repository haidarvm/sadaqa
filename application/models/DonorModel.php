<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class DonorModel extends CI_Model {
    private $tb_logging = 'logging';
    private $tb_donation = 'donation';
    private $tb_image_featured = 'image_featured';
    private $tb_donors = 'donors';
    private $tb_bank = 'bank';
    private $tb_moota = 'moota';



    public function getAllDonors() {
        // $this->db->select("*, d.donation_id as donation_id");
        $this->db->join('{PRE}bank as b', 'n.bank_id = b.bank_id', 'inner', false);
        $this->db->join('{PRE}donation as d', 'd.donation_id = n.donation_id', 'inner', false);
        // $this->db->group_by('d.donation_id');
        $this->db->order_by("donor_id", "DESC");
        $this->db->limit(100);
        $query =  $this->db->get_where($this->tb_donors. ' as n');
        
    //    echo $this->db->last_query();exit;
       return $query->result();
    }

    public function getLastDonorNum() {
        $this->db->limit(1);
        $this->db->order_by("code", "DESC");
        $date = date('Y-m-d');
        $query =  $this->db->get_where($this->tb_donors, ['DATE(created)' => $date]);
        // echo $this->db->last_query();exit;
        if($query->num_rows() > 0) {
            $code = $query->row();
            $last = $code + 1;
        } else {
            $last = 500;
        }
        return $last;
    }

    public function insertMoota($data) {
        // print_r($data);exit;
        $data['date'] = sqlDate($data['date']);
        // print_r($data);
        return $this->db->insert($this->tb_moota, $data);
    }

    public function getDonorTotal($amount) {
        $date = date('Y-m-d');
        $code = substr($amount, -3);
        $query =  $this->db->get_where($this->tb_donors, ['code' => $code, 'DATE(created)' => $date]);
        // echo $this->db->last_query();exit;
        return $query;
    }

}