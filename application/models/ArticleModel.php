<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ArticleModel extends CI_Model {
    private $tb_logging = 'logging';
    private $tb_article = 'article';
    private $tb_image_featured_article = 'image_featured_article';
    private $tb_image_center_article = 'image_center_article';



    public function getAllArticle() {
        $this->db->select("*, a.article_id as article_id");
          $this->db->join('{PRE}image_featured_article as i', 'i.article_id = a.article_id', 'left', false);
          $this->db->group_by('a.article_id');
          $query =  $this->db->get_where($this->tb_article. ' as a');
    //    echo $this->db->last_query();exit;
       return $query->result();
    }

    public function getArticle($article_id) {
    //    $this->db->select("img_mid, article_id, title, description, balance, bank_name, account_no, target, article.created ");
    //    $this->db->join('{PRE}image_featured as i', 'i.article_id = d.article_id', 'left', false);
       $query =  $this->db->get_where($this->tb_article , ['article_id' => $article_id]);
       return $query;
    }

    public function getDraft() {
        $query = $this->db->get_where($this->tb_article, ['status' => 0]);
        return $query;
    }

    public function newDraft() {
        $data = ['title' => '', 'description' => '', 'status' => '0', 'slug' => ''];
        $this->db->insert($this->tb_article, $data);
        return $this->db->insert_id();
    }

    public function getArticleImg($article_id) {
        $query =  $this->db->get_where($this->tb_image_featured_article, ['article_id' => $article_id]);
        //    echo $this->db->last_query();exit;
        return $query;
    }

    public function insert($data) {
        unset($data['article_id']);
        $data['slug'] = str_slug($data['title']);
        $data['status'] = 1;
        $query = $this->db->insert($this->tb_article, $data);
        return $this->db->insert_id();
    }

    public function update($data, $article_id) {
        $data['slug'] = str_slug($data['title']);
        $data['status'] = 1;
        return $this->db->update($this->tb_article, $data,['article_id' => $article_id] );
    }

    public function delete($article_id) {
        return $this->db->delete($this->tb_article, ['article_id' =>$article_id] );
    }

    public function insertImg($data) {
        $this->db->insert($this->tb_image_featured_article, $data);
        return $this->db->insert_id();
    }

    public function insertImgCenter($data) {
        $this->db->insert($this->tb_image_center_article, $data);
        return $this->db->insert_id();
    }

    public function updateImg($data,$image_featured_id) {
        // $id = $data['donation_id'];
        // unset($data['donation_id']);
        $query = $this->db->update($this->tb_image_featured_article, $data, ['image_featured_id' => $image_featured_id]);
        // echo  $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function updateImgCenter($data) {
        $id = $data['article_id'];
        unset($data['article_id']);
        $query = $this->db->update($this->tb_image_center_article, $data, ['article_id' => $id]);
        // echo  $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function deleteImg($image_featured_id) {
        return $this->db->delete('image_featured_article', ['image_featured_id' => $image_featured_id]);
    }

}