<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class SingleModel extends CI_Model {
    private $tb_image_featured_article = 'image_featured_article';
    private $tb_article = 'article';



    public function getArticle($article_id) {
        $this->db->join('{PRE}image_featured_article as i', 'i.article_id = a.article_id', 'left', false);
        $this->db->group_by('i.article_id');
        $query =  $this->db->get_where($this->tb_article. ' as a', ['a.article_id' => $article_id]);
  //    echo $this->db->last_query();exit;
        return $query->row();
    }


    public function getArticleSlug($slug) {
        $this->db->join('{PRE}image_featured_article as i', 'i.article_id = a.article_id', 'left', false);
        $this->db->group_by('i.article_id');
        $query =  $this->db->get_where($this->tb_article. ' as a', ['slug' => $slug]);
  //    echo $this->db->last_query();exit;
        return $query->row();
    }

    public function getLastArticle() {
        $this->db->select("*, a.article_id as article_id");
        $this->db->limit(10);
        $this->db->join('{PRE}image_featured_article as i', 'i.article_id = a.article_id', 'left', false);
        $this->db->group_by('i.article_id');
        $query = $this->db->get($this->tb_article. ' as a');
        return $query->result();
    }


}