<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class HomeModel extends CI_Model {
    private $tb_logging = 'logging';
    private $tb_donation = 'donation';
    private $tb_image_featured = 'image_featured';
    private $tb_donors = 'donors';



    public function getAllDonation() {
        $this->db->join('{PRE}image_featured as i', 'i.donation_id = d.donation_id', 'left', false);
        $this->db->group_by('d.donation_id');
        $query =  $this->db->get_where($this->tb_donation. ' as d');
  //    echo $this->db->last_query();exit;
     return $query->result();
  }


     public function getDonation($donation_id) {
    //    $this->db->select("img_mid, donation_id, title, description, balance, bank_name, account_no, target, donation.created ");
    //    $this->db->join('{PRE}image_featured as i', 'i.donation_id = d.donation_id', 'left', false);
       $query =  $this->db->get_where($this->tb_donation , ['donation_id' => $donation_id]);
       return $query;
    }

    public function getDonationSlug($slug) {
        // $this->db->join('{PRE}image_featured as i', 'i.donation_id = d.donation_id', 'left', false);
        // $this->db->group_by('d.donation_id');
        $query =  $this->db->get_where($this->tb_donation , ['slug' => $slug]);
        // echo $this->db->last_query();exit;
        return $query;
    }


    public function getLastArticle() {
        $this->db->select("*, a.article_id as article_id");
        $this->db->limit(10);
        $this->db->join('{PRE}image_featured_article as i', 'i.article_id = a.article_id', 'left', false);
        $this->db->group_by('i.article_id');
        $query = $this->db->get($this->tb_article. ' as a');
        return $query->result();
    }
    
    public function getSlidesBySlug($slug) {
        $this->db->join('{PRE}donation as d', 'i.donation_id = d.donation_id', 'left', false);
        $query =  $this->db->get_where($this->tb_image_featured. ' as i', ['slug' => $slug]);
        //    echo $this->db->last_query();exit;
        return $query;
    }

    public function getDonor($donors_id) {
        $this->db->join('{PRE}bank as b', 'n.bank_id = b.bank_id', 'inner', false);
        $query =  $this->db->get_where($this->tb_donors . ' as n', ['donor_id' => $donors_id]);
        // echo $this->db->last_query();exit;
        return $query;
    }

    public function getDonorByDonationId($donation_id) {
        $this->db->limit(5);
        $this->db->join('{PRE}moota as m', 'm.donation_id = d.donation_id', 'inner', false);
        $this->db->group_by('m.donation_id');
        $query =  $this->db->get_where($this->tb_donors . ' as d', ['m.donation_id' => $donation_id]);
        // echo $this->db->last_query();exit;
        return $query;
    }

    public function getDonorId($donors_id) {
        $this->db->join('{PRE}bank as b', 'n.bank_id = b.bank_id', 'inner', false);
        $query =  $this->db->get_where($this->tb_donors . ' as n' , ['donor_id' => $donors_id]);
        return $query;
    }

    public function getLastDonorNum() {
        $this->db->limit(1);
        $this->db->order_by("code", "DESC");
        $date = date('Y-m-d');
        $query =  $this->db->get_where($this->tb_donors, ['DATE(created)' => $date]);
        // echo $this->db->last_query();exit;
        if($query->num_rows() > 0) {
            $code = $query->row();
            $last = $code->code + 1;
        } else {
            $last = 500;
        }
        return $last;
    }


    public function insertDonor($data) {
        $this->db->insert($this->tb_donors, $data);
        return $this->db->insert_id();
    }
}