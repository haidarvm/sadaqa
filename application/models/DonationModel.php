<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class DonationModel extends CI_Model {
    private $tb_logging = 'logging';
    private $tb_donation = 'donation';
    private $tb_image_featured = 'image_featured';
    private $tb_donors = 'donors';



    public function getAllDonation() {
        $this->db->select("*, d.donation_id as donation_id");
          $this->db->join('{PRE}image_featured as i', 'i.donation_id = d.donation_id', 'left', false);
          $this->db->group_by('d.donation_id');
          $query =  $this->db->get_where($this->tb_donation. ' as d');
    //    echo $this->db->last_query();exit;
       return $query->result();
    }

    public function getDonation($donation_id) {
    //    $this->db->select("img_mid, donation_id, title, description, balance, bank_name, account_no, target, donation.created ");
    //    $this->db->join('{PRE}image_featured as i', 'i.donation_id = d.donation_id', 'left', false);
       $query =  $this->db->get_where($this->tb_donation , ['donation_id' => $donation_id]);
       return $query;
    }

    public function getDonationImg($donation_id) {
        $query =  $this->db->get_where($this->tb_image_featured, ['donation_id' => $donation_id]);
        //    echo $this->db->last_query();exit;
        return $query;
    }

    public function getAllDonors() {
        $query =  $this->db->get_where($this->tb_donation);
        return $query->result();
    }

    public function getDraft() {
        $query = $this->db->get_where($this->tb_donation, ['status' => 0]);
        return $query;
    }

    public function newDraft() {
        $data = ['title' => '', 'description' => '', 'status' => '0', 'slug' => ''];
        $this->db->insert($this->tb_donation, $data);
        return $this->db->insert_id();
    }

    public function insert($data) {
        unset($data['donataion_id']);
        $data['slug'] = str_slug($data['title']);
        $data['status'] = 1;
        $query = $this->db->insert($this->tb_donation, $data);
        return $this->db->insert_id();
    }

    public function update($data, $donation_id) {
        $data['slug'] = str_slug($data['title']);
        $data['status'] = 1;
        return $this->db->update($this->tb_donation, $data,['donation_id' => $donation_id] );
    }

    public function delete($donation_id) {
        return $this->db->delete($this->tb_donation, ['donation_id' =>$donation_id] );
    }

}